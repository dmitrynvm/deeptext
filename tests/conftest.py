import pytest
from deeptext import load


@pytest.fixture(scope='session')
def inp_data():
    """
    Loads the text data from the disk.
    """
    return load('data/input.txt')
