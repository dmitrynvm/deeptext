from deeptext import clean, check, sieve


def test_clean(inp_data):
    assert clean(inp_data[0]) == ['this', 'is', 'another']


def test_check(inp_data):
    assert check(clean(inp_data[0]), inp_data[-1][0]) == True


def test_reward(inp_data):
    assert len(sieve([clean(line) for line in inp_data])) == 2
