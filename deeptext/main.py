import sys
from typing import List, Dict


def load(filename: str) -> List[str]:
    """
    Load data from the text file in list format.

    :param filename: path to the file in the test format
    :return: list of strings loaded from the file
    """
    with open(filename, 'r') as f:
        outs = [out.strip() for out in f if out.strip()]
    return outs


def clean(line: str) -> List[Dict]:
    """
    Clean the line from the extra symbols.

    :param line: string of arbitrary symbols.
    :return: string of alphabet symbols.
    """
    return ''.join([s if s.isalpha() else ' ' for s in line]).split()


def check(line: List[str], term: str) -> List[Dict]:
    """
    Fast check whether the line contains the term.

    :param line: list of words contained in the line
    :param term: string that should be contained in line
    :return: boolean check result
    """

    for word in line:
        if term in word:
            return True
    return False


def sieve(lines: List[str]) -> List[str]:
    """
    Filters out the lines from the file with the term from the last line

    :param lines: contents of the loaded file
    :return: list of cleaned lines that contain the term
    """
    return [lines[i] for i in range(len(lines)-1) if check(lines[i], lines[-1][0])]


if __name__ == '__main__':
    lines = load(sys.argv[1])
    cleaned = [clean(line) for line in lines]
    sieved = sieve(cleaned)
    plain = '\n'.join(['[' + ' '.join(line) + ']' for line in sieved])
    print(plain)
