# DeepText

The application is a text file search engine operated by the following commands.
Algorithm is intended to be short, concise and testable.

Requirements: Python3, Bash.

1. Download and enter the repository
```bash
    git clone https://github.com/dmitrynvm/deeptext
    cd scrapao
```
2. Build and activate the environment
```bash
    make build
    source env/bin/activate
```
3. Run manually the app and check the json data
```bash
    make run
```
4. Run tests and ensure that everything is ok
```bash
    make test
```
5. Clean up the data and the environment
```bash
    make clean
    deactivate
    rm -rf env
```
