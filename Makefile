build:
	./script/build.sh

run:
	python3 deeptext/main.py data/input.txt

test:
	pytest --log-cli-level=INFO .

clean:
	./script/clean.sh
